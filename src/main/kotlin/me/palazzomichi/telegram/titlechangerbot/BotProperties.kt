package me.palazzomichi.telegram.titlechangerbot

import java.nio.file.Files.*
import java.nio.file.Path
import java.nio.file.Paths
import java.util.*

data class BotProperties(
        val token: String,
        val chatId: Long,
        val titlesPath: Path,
        val stringsPath: Path,
        val titleChangeDelay: Long
)

fun loadBotProperties(path: Path): BotProperties {
    val botProperties = Properties().apply {
        load(newInputStream(path))
    }
    val token = botProperties.getNotNullProperty("token")
    val chatId = botProperties.getNotNullProperty("chat-id").toLong()
    val titlesPath = botProperties.getNotNullProperty("titles-path")
    val stringsPath = botProperties.getNotNullProperty("strings-path")
    val titleChangeDelay = botProperties.getNotNullProperty("title-change-delay").toLong()
    return BotProperties(token, chatId, Paths.get(titlesPath), Paths.get(stringsPath), titleChangeDelay)
}

private fun Properties.getNotNullProperty(property: String) = getProperty(property)
        ?: throw MissingPropertyException(property)

fun BotProperties.save(path: Path) {
    createDirectories(path.parent)
    val botProperties = Properties().apply {
        setProperty("token", token)
        setProperty("chat-id", chatId.toString())
        setProperty("titles-path", titlesPath.toString())
        setProperty("strings-path", stringsPath.toString())
        setProperty("title-change-delay", titleChangeDelay.toString())
    }
    newOutputStream(path).use {
        botProperties.store(it, null)
    }
}

class MissingPropertyException(property: String) : Exception("cannot find property '$property'")
