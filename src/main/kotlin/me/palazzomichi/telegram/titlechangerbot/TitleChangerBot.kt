package me.palazzomichi.telegram.titlechangerbot

import io.github.ageofwar.telejam.Bot
import io.github.ageofwar.telejam.LongPollingBot
import io.github.ageofwar.telejam.chats.Chat
import io.github.ageofwar.telejam.commands.Command
import io.github.ageofwar.telejam.commands.CommandHandler
import io.github.ageofwar.telejam.connection.UploadFile
import io.github.ageofwar.telejam.messages.TextMessage
import io.github.ageofwar.telejam.text.Text
import java.io.FileNotFoundException
import java.nio.file.Path
import java.text.MessageFormat.format

class TitleChangerBot(
        bot: Bot,
        chat: Chat,
        titles: Titles,
        delay: Long,
        titlesPath: Path,
        strings: Strings
) : LongPollingBot(bot) {
    init {
        events.apply {
            val titleChanger = TitleChanger(bot, chat, titles.roundRobinIterator(), strings.titleChanger)
            val repeater = Repeater(delay, titleChanger::run)
            val errorMessage = Text.parseHtml(strings.errors.noPermission)
            val commands = strings.commands
            registerHelpCommand(bot, chat, commands.help)
            registerTitlesCommand(bot, chat, titlesPath, commands.titles, errorMessage)
            registerChangeTitleCommand(bot, chat, titleChanger, commands.changeTitle, errorMessage)
            registerEnableCommand(bot, chat, repeater, commands.enable, errorMessage)
            registerDisableCommand(bot, chat, repeater, commands.disable, errorMessage)
            registerAddTitlesCommand(bot, chat, titles, titlesPath, commands.addTitles, errorMessage)
            registerRemoveTitlesCommand(bot, chat, titles, titlesPath, commands.removeTitles, errorMessage)
            registerReloadTitlesCommand(bot, chat, titleChanger, titlesPath, commands.reloadTitles, errorMessage)
        }
    }
}

class HelpCommand(
        private val bot: Bot,
        private val chat: Chat,
        private val strings: Strings.Commands.Help
): CommandHandler {
    override fun onCommand(command: Command, message: TextMessage) {
        val member = bot.getChatMember(chat, message.sender)
        if (member.isAdmin) {
            bot.sendMessage(message, Text.parseHtml(strings.adminHelp))
        } else {
            bot.sendMessage(message, Text.parseHtml(strings.help))
        }
    }
}

class TitlesCommand(
        private val bot: Bot,
        private val titlesPath: Path,
        private val noTitlesMessage: String
) : CommandHandler {
    override fun onCommand(command: Command, message: TextMessage) {
        try {
            bot.sendDocument(message, UploadFile.fromFile(titlesPath.toString()))
        } catch (e: FileNotFoundException) {
            bot.sendMessage(message, Text.parseHtml(noTitlesMessage))
        }
    }
}

class EnableCommand(
        private val bot: Bot,
        private val repeater: Repeater,
        private val successMessage: String,
        private val alreadyEnabledMessage: String
) : CommandHandler {
    override fun onCommand(command: Command, message: TextMessage) {
        if (repeater.running) {
            bot.sendMessage(message, Text.parseHtml(alreadyEnabledMessage))
        } else {
            repeater.run()
            bot.sendMessage(message, Text.parseHtml(successMessage))
        }
    }
}

class DisableCommand(
        private val bot: Bot,
        private val repeater: Repeater,
        private val successMessage: String,
        private val alreadyDisabledMessage: String
) : CommandHandler {
    override fun onCommand(command: Command, message: TextMessage) {
        if (repeater.running) {
            repeater.stop()
            bot.sendMessage(message, Text.parseHtml(successMessage))
        } else {
            bot.sendMessage(message, Text.parseHtml(alreadyDisabledMessage))
        }
    }
}

class ChangeTitleCommand(private val titleChanger: TitleChanger) : CommandHandler {
    override fun onCommand(command: Command, message: TextMessage) {
        titleChanger.run()
    }
}

class AddTitlesCommand(
        private val bot: Bot,
        private val titles: Titles,
        private val titlesPath: Path,
        private val syntaxMessage: String,
        private val successMessage: String
) : CommandHandler {
    override fun onCommand(command: Command, message: TextMessage) {
        val args = command.args
        if (args.isEmpty) {
            bot.sendMessage(message, Text.parseHtml(syntaxMessage))
            return
        }
        val titles = args.toString().lines()
        this.titles.addAll(titles)
        this.titles.save(titlesPath)
        bot.sendMessage(message, Text.parseHtml(format(successMessage, titles.size)))
    }
}

class RemoveTitlesCommand(
        private val bot: Bot,
        private val titles: Titles,
        private val titlesPath: Path,
        private val syntaxMessage: String,
        private val successMessage: String
) : CommandHandler {
    override fun onCommand(command: Command, message: TextMessage) {
        val args = command.args
        if (args.isEmpty) {
            bot.sendMessage(message, Text.parseHtml(syntaxMessage))
            return
        }
        var removed = 0
        var notFound = 0
        for (title in args.toString().lineSequence()) {
            if (titles.remove(title)) {
                removed++
            } else {
                notFound++
            }
        }
        titles.save(titlesPath)
        bot.sendMessage(
                message,
                Text.parseHtml(format(successMessage, removed + notFound, removed, notFound))
        )
    }
}

class ReloadTitlesCommand(
        private val bot: Bot,
        private val titleChanger: TitleChanger,
        private val titlesPath: Path,
        private val successMessage: String
) : CommandHandler {
    override fun onCommand(command: Command, message: TextMessage) {
        val args = command.args.toString()
        val titles = loadTitles(titlesPath)
        val index = if (args.isEmpty()) 0 else titles.indexOf(args) + 1
        titleChanger.titleIterator = titles.roundRobinIterator(index)
        bot.sendMessage(message, Text.parseHtml(successMessage))
    }
}
