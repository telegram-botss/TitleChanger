package me.palazzomichi.telegram.titlechangerbot

import io.github.ageofwar.telejam.Bot
import java.nio.file.Files.createDirectories
import java.nio.file.Files.createFile
import java.nio.file.NoSuchFileException
import java.nio.file.Path
import java.nio.file.Paths
import java.util.concurrent.TimeUnit

private val BOT_PROPERTIES_PATH = Paths.get(".", "bot.properties")

fun main(vararg args: String) {
    try {
        val botProperties = loadBotProperties(BOT_PROPERTIES_PATH)
        val bot = Bot.fromToken(botProperties.token)
        val chat = bot.getChat(botProperties.chatId)
        val titles = tryLoadTitles(botProperties.titlesPath)
        val strings = tryLoadStrings(botProperties.stringsPath)
        val titleChangerBot = TitleChangerBot(
                bot, chat, titles, botProperties.titleChangeDelay, botProperties.titlesPath, strings
        )
        titleChangerBot.run()
    } catch (e: NoSuchFileException) {
        BotProperties(
                token = "123456:ABC-DEF1234ghIkl-zyx57W2v1u123ew11",
                chatId = -123456789L,
                titlesPath = Paths.get(".", "titles.txt"),
                stringsPath = Paths.get(".", "strings.json"),
                titleChangeDelay = TimeUnit.DAYS.toMillis(1)
        ).save(BOT_PROPERTIES_PATH)
    }
}

fun tryLoadStrings(path: Path) = try {
    loadStrings(path)
} catch (e: NoSuchFileException) {
    Strings().also {
        it.save(path)
    }
}

fun tryLoadTitles(path: Path) = try {
    loadTitles(path)
} catch (e: NoSuchFileException) {
    mutableListOf<String>().also {
        createDirectories(path.parent)
        createFile(path)
    }
}
