package me.palazzomichi.telegram.titlechangerbot

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import java.nio.charset.StandardCharsets.UTF_8
import java.nio.file.Files.*
import java.nio.file.Path

data class Strings(
        val errors: Errors = Errors(),
        val titleChanger: TitleChanger = TitleChanger(),
        val commands: Commands = Commands()
) {
    data class Errors(
            val noPermission: String = "<b>You have no permission to perform this command.</b>"
    )
    data class TitleChanger(
            val noPermission: String = "<b>This bot needs the 'can change info' permission in order to change the title.</b>",
            val noTitles: String = "<b>There are no titles saved yet!</b>\n Use /addtitle to add a title"
    )
    data class Commands(
            val help: Help = Help(),
            val titles: Titles = Titles(),
            val enable: Enable = Enable(),
            val disable: Disable = Disable(),
            val changeTitle: ChangeTitle = ChangeTitle(),
            val addTitles: AddTitles = AddTitles(),
            val removeTitles: RemoveTitles = RemoveTitles(),
            val reloadTitles: ReloadTitles = ReloadTitles()
    ) {
        data class Help(
                val name: String = "help",
                val aliases: List<String> = listOf("start"),
                val help: String = """
                    This bot changes the title of the group periodically.
                """.trimIndent(),
                val adminHelp: String = """
                    This bot changes the title of the group periodically.
                    Available commands:
                    - /titles shows all the saved titles
                    - /enable changes the title periodically
                    - /disable stops the bot changing titles
                    - /changetitle changes the title
                    - /addtitles &lt;title&gt; [\n title]... saves one or more titles
                    - /removetitles &lt;title&gt; [\n title]... removes one or more saved titles
                    - /reloadtitles [title] applies changes to the titles file (the next title will be the title after [title] or the first title)
                """.trimIndent()
        )
        data class Titles(
                val name: String = "titles",
                val aliases: List<String> = emptyList(),
                val noTitles: String = "<b>There are no titles saved yet!</b>\nUse /addtitle to add a title"
        )
        data class Enable(
                val name: String = "enable",
                val aliases: List<String> = emptyList(),
                val success: String = "<b>Titles enabled!</b>",
                val alreadyEnabled: String = "<b>Titles already enabled!</b>"
        )
        data class Disable(
                val name: String = "disable",
                val aliases: List<String> = emptyList(),
                val success: String = "<b>Titles disabled!</b>",
                val alreadyDisabled: String = "<b>Titles already disabled!</b>"
        )
        data class ChangeTitle(
                val name: String = "changetitle",
                val aliases: List<String> = emptyList()
        )
        data class AddTitles(
                val name: String = "addtitles",
                val aliases: List<String> = listOf("addtitle"),
                val syntax: String = "<b>Syntax:</b> <code>/addtitle &lt;title&gt;[\\n titles]...</code>",
                val success: String = "<b>{0} title{0, choice, 1#| 2#s} added!</b>"
        )
        data class RemoveTitles(
                val name: String = "removetitles",
                val aliases: List<String> = listOf("removetitle"),
                val syntax: String = "<b>Syntax:</b> <code>/removetitle &lt;title&gt;[\\n titles]...</code>",
                val success: String = "<b>{1} title{1, choice, 0#s| 1#| 2#s} removed.{2, choice, 0#| 1#\n'{2}' title'{2, choice, 0#s| 1#| 2#s}' not found.}</b>"
        )
        data class ReloadTitles(
                val name: String = "reloadtitles",
                val aliases: List<String> = emptyList(),
                val success: String = "<b>Titles reloaded!</b>"
        )
    }
}

fun loadStrings(path: Path): Strings {
    return Gson().fromJson(newBufferedReader(path), Strings::class.java)
}

fun Strings.save(path: Path) {
    createDirectories(path.parent)
    newBufferedWriter(path, UTF_8).use {
        GsonBuilder()
                .setPrettyPrinting()
                .disableHtmlEscaping()
                .create()
                .toJson(this, it)
    }
}
