package me.palazzomichi.telegram.titlechangerbot

import java.util.concurrent.Executors
import java.util.concurrent.ScheduledFuture
import java.util.concurrent.TimeUnit

class Repeater(private val delay: Long, private val action: () -> Unit) : Runnable {
    private val executor = Executors.newSingleThreadScheduledExecutor()
    private var future: ScheduledFuture<*>? = null

    val running get() = future != null

    override fun run() {
        check(!running)
        future = executor.scheduleAtFixedRate(action, 0L, delay, TimeUnit.MILLISECONDS)
    }

    fun stop() {
        check(running)
        future!!.cancel(false)
        future = null
    }
}