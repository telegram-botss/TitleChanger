package me.palazzomichi.telegram.titlechangerbot

import io.github.ageofwar.telejam.Bot
import io.github.ageofwar.telejam.chats.Chat
import io.github.ageofwar.telejam.connection.UploadFile
import io.github.ageofwar.telejam.messages.*
import io.github.ageofwar.telejam.methods.*
import io.github.ageofwar.telejam.text.Text
import io.github.ageofwar.telejam.users.*

fun Bot.getChat(id: Long): Chat {
    val getChat = GetChat()
            .chat(id)
    return execute(getChat)
}

fun Bot.canChangeInfo(chat: Chat): Boolean {
    val getChatMember = GetChatMember()
            .chat(chat)
            .user(id)
    return execute(getChatMember).canChangeInformation()
}

fun Bot.sendMessage(replyToMessage: Message, text: Text): TextMessage {
    val sendMessage = SendMessage()
            .replyToMessage(replyToMessage)
            .text(text)
    return execute(sendMessage)
}

fun Bot.sendMessage(chat: Chat, text: Text): TextMessage {
    val sendMessage = SendMessage()
            .chat(chat)
            .text(text)
    return execute(sendMessage)
}

fun Bot.setChatTitle(chat: Chat, title: String) {
    val setChatTitle = SetChatTitle()
            .chat(chat)
            .title(title)
    execute(setChatTitle)
}

fun Bot.getChatMember(chat: Chat, user: User): ChatMember {
    val getChatMember = GetChatMember()
            .chat(chat)
            .user(user)
    return execute(getChatMember)
}

fun Bot.sendDocument(replyToMessage: Message, document: UploadFile): DocumentMessage {
    val sendDocument = SendDocument()
            .replyToMessage(replyToMessage)
            .document(document)
    return execute(sendDocument)
}
