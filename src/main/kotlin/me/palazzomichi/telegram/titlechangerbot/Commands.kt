package me.palazzomichi.telegram.titlechangerbot

import io.github.ageofwar.telejam.Bot
import io.github.ageofwar.telejam.chats.Chat
import io.github.ageofwar.telejam.commands.CommandHandler
import io.github.ageofwar.telejam.events.EventRegistry
import io.github.ageofwar.telejam.text.Text
import java.nio.file.Path

fun EventRegistry.registerHelpCommand(
        bot: Bot,
        chat: Chat,
        strings: Strings.Commands.Help) {
    registerCommand(
            HelpCommand(bot, chat, strings),
            strings.name, *strings.aliases.toTypedArray()
    )
}

fun EventRegistry.registerTitlesCommand(bot: Bot,
                                        chat: Chat,
                                        titlesPath: Path,
                                        strings: Strings.Commands.Titles,
                                        errorMessage: Text) {
    registerCommand(
            TitlesCommand(bot, titlesPath, strings.noTitles).adminCommand(chat, bot, errorMessage),
            strings.name, *strings.aliases.toTypedArray()
    )
}

fun EventRegistry.registerChangeTitleCommand(bot: Bot,
                                             chat: Chat,
                                             titleChanger: TitleChanger,
                                             strings: Strings.Commands.ChangeTitle,
                                             errorMessage: Text) {
    registerCommand(
            ChangeTitleCommand(titleChanger).adminCommand(chat, bot, errorMessage),
            strings.name, *strings.aliases.toTypedArray()
    )
}

fun EventRegistry.registerEnableCommand(bot: Bot,
                                        chat: Chat,
                                        repeater: Repeater,
                                        strings: Strings.Commands.Enable,
                                        errorMessage: Text) {
    registerCommand(
            EnableCommand(bot, repeater, strings.success, strings.alreadyEnabled).adminCommand(chat, bot, errorMessage),
            strings.name, *strings.aliases.toTypedArray()
    )
}

fun EventRegistry.registerDisableCommand(bot: Bot,
                                         chat: Chat,
                                         repeater: Repeater,
                                         strings: Strings.Commands.Disable,
                                         errorMessage: Text) {
    registerCommand(
            DisableCommand(bot, repeater, strings.success, strings.alreadyDisabled).adminCommand(chat, bot, errorMessage),
            strings.name, *strings.aliases.toTypedArray()
    )
}

fun EventRegistry.registerAddTitlesCommand(bot: Bot,
                                           chat: Chat,
                                           titles: Titles,
                                           titlesPath: Path,
                                           strings: Strings.Commands.AddTitles,
                                           errorMessage: Text) {
    registerCommand(
            AddTitlesCommand(bot, titles, titlesPath, strings.syntax, strings.success).adminCommand(chat, bot, errorMessage),
            strings.name, *strings.aliases.toTypedArray()
    )
}

fun EventRegistry.registerRemoveTitlesCommand(bot: Bot,
                                              chat: Chat,
                                              titles: Titles,
                                              titlesPath: Path,
                                              strings: Strings.Commands.RemoveTitles,
                                              errorMessage: Text) {
    registerCommand(
            RemoveTitlesCommand(bot, titles, titlesPath, strings.syntax, strings.success).adminCommand(chat, bot, errorMessage),
            strings.name, *strings.aliases.toTypedArray()
    )
}

fun EventRegistry.registerReloadTitlesCommand(bot: Bot,
                                              chat: Chat,
                                              titleChanger: TitleChanger,
                                              titlesPath: Path,
                                              strings: Strings.Commands.ReloadTitles,
                                              errorMessage: Text) {
    registerCommand(
            ReloadTitlesCommand(bot, titleChanger, titlesPath, strings.success).adminCommand(chat, bot, errorMessage),
            strings.name, *strings.aliases.toTypedArray()
    )
}

fun CommandHandler.adminCommand(chat: Chat, bot: Bot, errorMessage: Text): CommandHandler {
    return CommandHandler { command, message ->
        val member = bot.getChatMember(chat, message.sender)
        if (member.isAdmin) {
            this.onCommand(command, message)
        } else {
            bot.sendMessage(message, errorMessage)
        }
    }
}
