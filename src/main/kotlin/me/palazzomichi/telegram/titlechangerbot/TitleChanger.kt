package me.palazzomichi.telegram.titlechangerbot

import io.github.ageofwar.telejam.Bot
import io.github.ageofwar.telejam.chats.Chat
import io.github.ageofwar.telejam.text.Text

class TitleChanger(
        private val bot: Bot,
        private val chat: Chat,
        var titleIterator: Iterator<String>,
        private val strings: Strings.TitleChanger
) : Runnable {
    override fun run() {
        if (titleIterator.hasNext()) {
            tryChangeTitle(titleIterator.next())
        } else {
            bot.sendMessage(chat, Text.parseHtml(strings.noTitles))
        }
    }

    fun tryChangeTitle(title: String) {
        if (bot.canChangeInfo(chat)) {
            bot.setChatTitle(chat, title)
        } else {
            bot.sendMessage(chat, Text.parseHtml(strings.noPermission))
        }
    }
}
