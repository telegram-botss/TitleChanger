package me.palazzomichi.telegram.titlechangerbot

import java.nio.charset.StandardCharsets.UTF_8
import java.nio.file.Files.*
import java.nio.file.Path
import java.util.concurrent.atomic.AtomicInteger

typealias Titles = MutableList<String>

fun <T> MutableList<T>.roundRobinIterator(start: Int = 0) = object : MutableListIterator<T> {
    private var index = AtomicInteger(start)
    private var back = false

    override fun hasNext() = isNotEmpty()

    override fun next() = try {
        get(index.getAndIncrement() % size).also {
            back = false
        }
    } catch (e: IndexOutOfBoundsException) {
        throw NoSuchElementException(e.message)
    }

    override fun nextIndex() = index.get()

    override fun hasPrevious() = isNotEmpty()

    override fun previous() = try {
        get(index.decrementAndGet() % size).also {
            back = true
        }
    } catch (e: IndexOutOfBoundsException) {
        throw NoSuchElementException(e.message)
    }

    override fun previousIndex() = index.get() - 1

    override fun add(element: T) {
        add(index.get(), element)
        index.incrementAndGet()
    }

    private fun lastIndex() = if (back) nextIndex() else previousIndex()

    override fun remove() {
        removeAt(lastIndex())
    }

    override fun set(element: T) {
        set(lastIndex(), element)
    }
}

fun loadTitles(path: Path): Titles {
    return readAllLines(path)
}

fun Titles.save(path: Path) {
    createDirectories(path.parent)
    write(path, this, UTF_8)
}
